#!/usr/bin/env bash
# Terminate already running bar instances
killall -qw picom

# Launch picom
exec picom --config ~/.config/i3/picom &

echo "Picom launched..."
