set nocompatible
filetype off
syntax on

" set guifont=DejaVu\ Sans:s12

" Ignore text case
set ignorecase
set smartcase

" set number
" filetype plugin on

"USE THE FORCE LUKE"
nnoremap <Left> :echoe "Use h"<CR>
nnoremap <Right> :echoe "Use l"<CR>
nnoremap <Up> :echoe "Use k"<CR>
nnoremap <Down> :echoe "Use j"<CR>
imap jj <Esc>
" "/USE THE FORCE"

set tabpagemax=100

" Tabs
nnoremap gr gT

set backupdir=~/.vim/backup/
set directory=~/.vim/backup/

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" Let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" Color scheme
Plugin 'dracula/vim'
colorscheme dracula
set background=dark

" Git branch info
Plugin 'tpope/vim-fugitive'

" AirLIne
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
let g:airline_theme = "dracula"
let g:airline#extensions#tabline#enabled = 1
" let g:airline_extensions = ['airline-ale']
" let g:airline#extensions#ale#enabled = 1
let g:airline#extensions#branch#enabled = 1

" air-line
let g:airline_powerline_fonts = 1

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
    endif

" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.whitespace = 'Ξ'

" airline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''
""""""""" /AirLine

" Tree file viewer
Plugin 'scrooloose/nerdtree'

" FZF Search
Plugin 'junegunn/fzf'
Plugin 'junegunn/fzf.vim'
nmap <C-f> :Files<CR>
nmap <silent> <Leader>f :Rg<CR>

" MarkDown viewer glow
Plugin 'skanehira/preview-markdown.vim'
let g:preview_markdown_parser = 'glow'
let g:preview_markdown_auto_update = '1'
nmap <C-p> :PreviewMarkdown tab<CR>

" Ansible
Plugin 'pearofducks/ansible-vim'
let g:ansible_unindent_after_newline = 1

" Pass the path to set the runtimepath properly.
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
