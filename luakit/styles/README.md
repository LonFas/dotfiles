Then create a css file - like forcefont.css:

```conf
 @-moz-document regexp(".*") {
   * {
     font-family: monospace !important;
     font-size: 16px !important;
     color: red !important;
   }
 }
```

This overrides font family, size and color. Adjust to your liking.

If you want to override selected websites only, change the first line into something like @-moz-document regexp(".*github.com.*") {

Best Regards,
Stefan

---

<https://luakit.github.io/docs/pages/02-faq.html>

set webview.zoom_level 150
seton example.com webview.zoom_level 200

seton habr.com webview.zoom_level 130
seton 3dnews.ru webview.zoom_level 130
seton opennet.ru webview.zoom_level 120
